package org.fasilkom.psp.tugas1.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PesertaModel {
	
	private String nomor;
	private String nama;
	private Date tglLahir;
	private ProdiModel prodi;

}
