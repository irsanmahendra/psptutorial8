package org.fasilkom.psp.tugas1.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UnivModel {
	
	private String kodeUniv;
	private String namaUniv;
	private String urlUniv;
	private List<ProdiModel> daftarProdi;
}
