package org.fasilkom.psp.tugas1.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProdiModel {

	private UnivModel univ;
	private String kodeProdi;
	private String namaProdi;
	private List<PesertaModel> daftarPeserta;
	
}
