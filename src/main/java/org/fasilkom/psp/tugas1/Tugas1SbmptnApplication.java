package org.fasilkom.psp.tugas1;

import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Tugas1SbmptnApplication {
	
	@Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {
            System.out.println("Let's inspect the beans provided by Spring Boot:");

            String[] beanNames = ctx.getBeanDefinitionNames();
            Arrays.sort(beanNames);
            System.out.print("[");
            for (String beanName : beanNames) {
                System.out.print(beanName + ", ");
            }
            System.out.print("]");
            System.out.println();

        };
    }
	
	public static void main(String[] args) {
		SpringApplication.run(Tugas1SbmptnApplication.class, args);
	}
}
