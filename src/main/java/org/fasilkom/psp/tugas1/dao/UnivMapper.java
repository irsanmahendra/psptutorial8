package org.fasilkom.psp.tugas1.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.fasilkom.psp.tugas1.model.ProdiModel;
import org.fasilkom.psp.tugas1.model.UnivModel;

@Mapper
public interface UnivMapper {

	final String QUERY_ADD_UNIV = "insert into univ (kode_univ, nama_univ, url_univ) VALUES (#{kodeUniv}, #{namaUniv}, #{urlUniv})";
	final String QUERY_DELETE_UNIV = "delete from univ where kode_univ = #{kodeUniv}";
	final String QUERY_UPDATE_UNIV = "update peserta set nama = #{nama}, tgl_lahir = #{tglLahir}, kodeProdi= #{prodi.kodeProdi} where nomor = #{nomor}";
	final String QUERY_SELECT_UNIV = "select kode_univ, nama_univ, url_univ from univ where kode_univ = #{kodeUniv}";
	final String QUERY_SELECT_ALL_UNIV = "select kode_univ, nama_univ, url_univ from univ";
	
	final String QUERY_SELECT_PRODI = "select kode_prodi, nama_prodi from prodi where kode_univ = #{kodeUniv}";

	@Select(QUERY_SELECT_UNIV)
	@Results(value = { 
			@Result(property = "kodeUniv", column = "kode_univ"),
			@Result(property = "namaUniv", column = "nama_univ"),
			@Result(property = "urlUniv", column = "url_univ"),
			@Result(property = "daftarProdi", column = "kode_univ", 
				javaType = List.class, 
				many = @Many(select = "selectDaftarProdi"))
		})
	UnivModel selectUniv(@Param("kodeUniv") String kodeUniv);
	
	@Select(QUERY_SELECT_ALL_UNIV)
	@Results(value = { 
			@Result(property = "kodeUniv", column = "kode_univ"),
			@Result(property = "namaUniv", column = "nama_univ"),
			@Result(property = "urlUniv", column = "url_univ")
		})
	List<UnivModel> selectAllUniv();
	
	@Insert(QUERY_ADD_UNIV)
	void addUniv(UnivModel univ);

	@Delete(QUERY_DELETE_UNIV)
	void deleteUniv(@Param("kodeUniv") String kodeUniv);

	@Update(QUERY_UPDATE_UNIV)
	void updateUniv(UnivModel univ);
	
	@Select(QUERY_SELECT_PRODI)
	@Results(value = {
			@Result(property = "kodeProdi", column = "kode_prodi"),
			@Result(property = "namaProdi", column = "nama_prodi")
		})
	List<ProdiModel> selectDaftarProdi(String kodeUniv);
	
	
}
