package org.fasilkom.psp.tugas1.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.fasilkom.psp.tugas1.model.PesertaModel;
import org.fasilkom.psp.tugas1.model.ProdiModel;
import org.fasilkom.psp.tugas1.model.UnivModel;

@Mapper
public interface ProdiMapper {

	final String QUERY_ADD_PRODI = "insert into prodi (kode_univ, kode_prodi, nama_prodi) VALUES (#{univ.kodeUniv}, #{kodeProdi}, #{namaProdi})";
	final String QUERY_DELETE_PRODI = "delete from prodi where kode_univ = #{kodeUniv} and kode_prodi = #{kodeProdi}";
	final String QUERY_UPDATE_PRODI = "update prod set nama_prodi = #{namaProdi} where kode_univ = #{univ.kodeUniv} and kode_prodi = #{kodeProdi}";
	final String QUERY_SELECT_PRODI = "select kode_univ, kode_prodi, nama_prodi from prodi where kode_prodi = #{kodeProdi}";
	final String QUERY_SELECT_ALL_PRODI = "select kode_univ, kode_prodi, nama_prodi from prodi ";
	
	@Select(QUERY_SELECT_PRODI)
	@Results(value = { 
			@Result(property = "kodeProdi", column = "kode_prodi"),
			@Result(property = "namaProdi", column = "nama_prodi"),
			@Result(property = "univ", column = "kode_univ", javaType = UnivModel.class, one = @One(select = "selectUniv")),
			@Result(property = "daftarPeserta", column = "kode_prodi", 
				javaType = List.class, 
				many = @Many(select = "selectPeserta"))
		})
	ProdiModel selectProdi(@Param("kodeProdi") String kodeProdi);
	
	@Select(QUERY_SELECT_ALL_PRODI)
	@Results(value = { 
//			@Result(property = "univ.kodeUniv", column = "kode_univ"),
			@Result(property = "kodeProdi", column = "kode_prodi"),
			@Result(property = "namaProdi", column = "nama_prodi"),
			@Result(property = "univ", column = "kode_univ", javaType = UnivModel.class, one = @One(select = "selectUniv"))
		})
	List<ProdiModel> selectAllProdi();
	
	@Insert(QUERY_ADD_PRODI)
	void addProdi(ProdiModel prodi);

	@Delete(QUERY_DELETE_PRODI)
	void deleteProdi(@Param("kodeUniv") String kodeUniv, @Param("kodeProdi") String kodeProdi);

	@Update(QUERY_UPDATE_PRODI)
	void updateProdi(ProdiModel prodi);
	
	@Select("select nomor, nama, tgl_lahir from peserta "
			+ "join prodi on peserta.kode_prodi = prodi.kode_prodi "
			+ "where prodi.kode_prodi = #{kodeProdi}")
	List<PesertaModel> selectPeserta(@Param("kodeProdi") String kodeProdi);
	
	@Results(value = { 
			@Result(property = "kodeUniv", column = "kode_univ"),
			@Result(property = "urlUniv", column = "url_univ"),
			@Result(property = "namaUniv", column = "nama_univ")
	})
	@Select("select kode_univ, nama_univ, url_univ from univ where kode_univ = #{kodeUniv}")
	UnivModel selectUniv(@Param("kodeUniv") String kodeUniv);
}
