package org.fasilkom.psp.tugas1.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.One;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.fasilkom.psp.tugas1.model.PesertaModel;
import org.fasilkom.psp.tugas1.model.ProdiModel;;


@Mapper
public interface PesertaMapper {

	final String QUERY_ADD_PESERTA = "insert into peserta (nomor, nama, tgl_lahir, kode_prodi) VALUES (#{nomor}, #{nama}, #{tglLahir}, #{prodi.kodeProdi}))";
	final String QUERY_DELETE_PESERTA = "delete from peserta where nomor = #{nomor}";
	final String QUERY_UPDATE_PESERTA = "update peserta set nama = #{nama}, tgl_lahir = #{tglLahir}, kodeProdi= #{prodi.kodeProdi} where nomor = #{nomor}";
	final String QUERY_SELECT_PESERTA = "select nomor, nama, tgl_lahir, kode_prodi from peserta where nomor = #{nomor}";
	final String QUERY_SELECT_ALL_PESERTA = "select nomor, nama, tgl_lahir, kode_prodi from peserta";
	final String QUERY_CHANGE_PRODI_PESERTA = "update peserta set kode_prodi = #{prodi.kodeProdi} where nomor=#{nomor}";
	
	final String QUERY_SELECT_ALL_PESERTA_BY_UNIV = 
			"select p.nomor, p.nama, p.tgl_lahir, pr.kode_prodi, pr.nama_prodi, u.kode_univ, u.nama_univ "
			+ "from peserta p "
			+ "join prodi pr on p.kode_prodi = pr.kode_prodi "
			+ "join univ u on pr.kode_univ = u.kode_univ "
			+ "where u.kode_univ = #{kodeUniv} "
			+ "order by p.nomor asc;";
	
	
	final String QUERY_SELECT_PRODI = "select p.kode_univ, p.kode_prodi, p.nama_prodi, u.nama_univ, u.url_univ from prodi p join univ u on p.kode_univ = u.kode_univ where p.kode_prodi = #{kodeProdi}";
	
	
	@Select(QUERY_SELECT_PESERTA)
	@Results(value = { 
			@Result(property = "nomor", column = "nomor"),
			@Result(property = "nama", column = "name"),
			@Result(property = "tglLahir", column = "tgl_lahir"),
			@Result(property = "prodi", column = "kode_prodi", javaType = ProdiModel.class, one = @One(select = "selectProdi"))
		})
	PesertaModel selectPeserta(@Param("nomor") String nomor);
	
	@Select(QUERY_SELECT_ALL_PESERTA)
	@Results(value = { 
			@Result(property = "nomor", column = "nomor"),
			@Result(property = "nama", column = "name"),
			@Result(property = "tglLahir", column = "tgl_lahir"),
			@Result(property = "prodi", column = "kode_prodi", javaType = ProdiModel.class, one = @One(select = "selectProdi"))
		})
	List<PesertaModel> selectAllPeserta();
	
	@Select(QUERY_SELECT_ALL_PESERTA_BY_UNIV)
	@Results(value = { 
			@Result(property = "nomor", column = "nomor"),
			@Result(property = "nama", column = "name"),
			@Result(property = "tglLahir", column = "tgl_lahir"),
			@Result(property = "prodi.kodeProdi", column = "kode_prodi"),
			@Result(property = "prodi.namaProdi", column = "nama_prodi"),
			@Result(property = "prodi.univ.kodeUniv", column = "kode_univ"),
			@Result(property = "prodi.univ.namaUniv", column = "nama_univ")
		})
	List<PesertaModel> selectAllPesertaByUniv(String kodeUniv);
	
	@Insert(QUERY_ADD_PESERTA)
	void addPeserta(PesertaModel peserta);

	@Delete(QUERY_DELETE_PESERTA)
	void deletePeserta(@Param("nomor") String nomor);

	@Update(QUERY_UPDATE_PESERTA)
	void updatePeserta(PesertaModel peserta);
	
	@Update(QUERY_CHANGE_PRODI_PESERTA)
	void changeProdiPeserta(PesertaModel peserta);
	
	@Select(QUERY_SELECT_PRODI)
	@Results(value = { 
			@Result(property = "kodeProdi", column = "kode_prodi"),
			@Result(property = "namaProdi", column = "nama_prodi"),
			@Result(property = "univ.kodeUniv", column = "kode_univ"),
			@Result(property = "univ.namaUniv", column = "nama_univ"),
			@Result(property = "univ.urlUniv", column = "url_univ"),
			
		})
	ProdiModel selectProdi(@Param("kodeProdi") String kodeProdi);
}
