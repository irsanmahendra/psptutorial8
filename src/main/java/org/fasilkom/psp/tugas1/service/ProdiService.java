package org.fasilkom.psp.tugas1.service;

import java.util.List;

import org.fasilkom.psp.tugas1.model.ProdiModel;

public interface ProdiService {

	public List<ProdiModel> selectAll();
	public ProdiModel selectProdi(String kodeProdi);
	public void insertProdi(ProdiModel prodi);
}
