package org.fasilkom.psp.tugas1.service.impl;

import java.util.List;

import org.fasilkom.psp.tugas1.dao.ProdiMapper;
import org.fasilkom.psp.tugas1.model.ProdiModel;
import org.fasilkom.psp.tugas1.service.ProdiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ProdiServiceDatabase implements ProdiService{
	
	@Autowired
	private ProdiMapper prodiMapper;
	
	public List<ProdiModel> selectAll() {
		log.info("select all prodi");
		List<ProdiModel> daftarProdi = prodiMapper.selectAllProdi();
		return daftarProdi;
	}
	
	@Override
	public ProdiModel selectProdi(String kodeProdi){
		log.info("select prodi with kode="+kodeProdi);
		ProdiModel prodi = prodiMapper.selectProdi(kodeProdi);
		return prodi;
	}
	
	@Override
	public void insertProdi(ProdiModel prodi){
		log.info("insert prodi with kodeProdi["+prodi.getKodeProdi()+"], namaProdi["+prodi.getNamaProdi()+"], univ["+prodi.getUniv().getKodeUniv()+","+prodi.getUniv().getNamaUniv()+"]");
		prodiMapper.addProdi(prodi);
	}
}
