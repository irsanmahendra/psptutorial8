package org.fasilkom.psp.tugas1.service;

import java.util.Date;
import java.util.List;

import org.fasilkom.psp.tugas1.model.PesertaModel;

public interface PesertaService {

	public PesertaModel inquiryPeserta(String nomor);
	public Integer hitungUmur(Date tglLahir);
	public List<PesertaModel> selectAllPesertaByUniv(String kodeUniv);
	public void changeProdi(PesertaModel peserta);
}
