package org.fasilkom.psp.tugas1.service.impl;

import java.util.Date;
import java.util.List;

import org.fasilkom.psp.tugas1.dao.PesertaMapper;
import org.fasilkom.psp.tugas1.model.PesertaModel;
import org.fasilkom.psp.tugas1.service.PesertaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.joda.time.LocalDate;
import org.joda.time.Years;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PersertaServiceDatabase implements PesertaService {

	@Autowired
	private PesertaMapper pesertaMapper;
	
	public PesertaModel inquiryPeserta(String nomor){
		log.info("inquiry peserta with nomor["+nomor+"]");
		PesertaModel peserta = pesertaMapper.selectPeserta(nomor);
	
		return peserta;
	}
	
	public List<PesertaModel> selectAllPesertaByUniv(String kodeUniv){
		log.info("select all peserta by univ = "+kodeUniv);
		List<PesertaModel> daftarPeserta = pesertaMapper.selectAllPesertaByUniv(kodeUniv);
		return daftarPeserta;
	}
	
	public void changeProdi(PesertaModel peserta){
		log.info("change prodi peserta ["+peserta.getNomor()+","+peserta.getNama()+"]");
		pesertaMapper.changeProdiPeserta(peserta);
	}
	
	public Integer hitungUmur(Date date){
		LocalDate birthday = new LocalDate(date);
		LocalDate now = new LocalDate();
		Years age = Years.yearsBetween(birthday, now);
		return age.getYears();
	}

}