package org.fasilkom.psp.tugas1.service;

import java.util.List;

import org.fasilkom.psp.tugas1.model.UnivModel;

public interface UnivService {

	public List<UnivModel> getAllUniv();
	public UnivModel selectUniv(String kodeUniv);
	
}
