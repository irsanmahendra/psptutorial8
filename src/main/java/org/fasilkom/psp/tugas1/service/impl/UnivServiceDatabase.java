package org.fasilkom.psp.tugas1.service.impl;

import java.util.List;

import org.fasilkom.psp.tugas1.dao.UnivMapper;
import org.fasilkom.psp.tugas1.model.UnivModel;
import org.fasilkom.psp.tugas1.service.UnivService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UnivServiceDatabase implements UnivService {

	@Autowired
	private UnivMapper univMapper;
	
	@Override
	public List<UnivModel> getAllUniv(){
		log.info("get all univ");
		return univMapper.selectAllUniv();
	}
	
	@Override
	public UnivModel selectUniv(String kodeUniv){
		log.info("select univ with kode="+kodeUniv);
		UnivModel univ = univMapper.selectUniv(kodeUniv);
		return univ;
	}
}
