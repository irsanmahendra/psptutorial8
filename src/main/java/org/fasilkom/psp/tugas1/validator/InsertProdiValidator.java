package org.fasilkom.psp.tugas1.validator;

import javax.management.RuntimeErrorException;

import org.fasilkom.psp.tugas1.model.ProdiModel;
import org.fasilkom.psp.tugas1.service.ProdiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class InsertProdiValidator implements Validator{

	@Autowired
	private ProdiService prodiService;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return ProdiModel.class.equals(clazz);
	}

	@Override
	public void validate(Object obj, Errors err) {
		
		ProdiModel prodi = (ProdiModel) obj;
		if(prodi == null) throw new RuntimeErrorException(new Error("new Error"), "koment");
		ProdiModel selectProdi = prodiService.selectProdi(prodi.getKodeProdi());
		if(selectProdi != null){
			throw new RuntimeErrorException(new Error("new Error"), "kode prodi sudah terdaftar");
		}
	}

}
