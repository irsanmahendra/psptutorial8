package org.fasilkom.psp.tugas1.controller;

import java.util.List;

import org.fasilkom.psp.tugas1.model.PesertaModel;
import org.fasilkom.psp.tugas1.model.UnivModel;
import org.fasilkom.psp.tugas1.service.PesertaService;
import org.fasilkom.psp.tugas1.service.UnivService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class UnivController {

	@Autowired
	private UnivService univService;
	
	@Autowired
	private PesertaService pesertaService;

	@RequestMapping("/univ")
	public String univ(Model model){
		List<UnivModel> daftarUniv = univService.getAllUniv();
		model.addAttribute("daftarUniv", daftarUniv);
		return "univ";
	}
	
	@RequestMapping("/univ/{kodeUniv}")
    public String view (Model model,
            @PathVariable(value = "kodeUniv") String kodeUniv)
    {
    	UnivModel univ = univService.selectUniv(kodeUniv);
    	
    	if(univ != null) {
    		model.addAttribute("univ", univ);
    		return "univ-view";
    	} else{
    		model.addAttribute("kodeUniv", kodeUniv);
    		return "univ-not-found";
    	}
    }
	
	@RequestMapping("/univ/{kodeUniv}/peserta")
    public String viewAllPeserta (Model model,
            @PathVariable(value = "kodeUniv") String kodeUniv)
    {
    	UnivModel univ = univService.selectUniv(kodeUniv);
    	
    	if(univ != null) {
    		List<PesertaModel> daftarPeserta = pesertaService.selectAllPesertaByUniv(kodeUniv);
    		model.addAttribute("univ", univ);
    		model.addAttribute("daftarPeserta", daftarPeserta);
    		
    		return "univ-peserta-view";
    	} else{
    		model.addAttribute("kodeUniv", kodeUniv);
    		return "univ-not-found";
    	}
    }
}
