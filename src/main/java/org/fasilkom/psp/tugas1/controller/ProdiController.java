package org.fasilkom.psp.tugas1.controller;

import java.util.List;

import org.fasilkom.psp.tugas1.model.ProdiModel;
import org.fasilkom.psp.tugas1.model.UnivModel;
import org.fasilkom.psp.tugas1.service.ProdiService;
import org.fasilkom.psp.tugas1.service.UnivService;
import org.fasilkom.psp.tugas1.validator.InsertProdiValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class ProdiController {

	@Autowired
	private ProdiService prodiService;
	
	@Autowired
	private UnivService univService;
	
	@Autowired
	private InsertProdiValidator validator;
	
	@RequestMapping("/prodi")
    public String viewProdi (Model model,
           @RequestParam("kode") String kode)
    {
    	ProdiModel prodi = prodiService.selectProdi(kode);
    	
    	if(prodi != null) {
    		model.addAttribute("prodi", prodi);
    		return "prodi-view";
    	} else{
    		model.addAttribute("kodeProdi", kode);
    		return "prodi-not-found";
    	}
    }
	
	@RequestMapping("/prodi/insert")
    public String insert(Model model)
    {
		List<UnivModel> daftarUniv = univService.getAllUniv();
		ProdiModel prodi = new ProdiModel();
		model.addAttribute("prodi", prodi);
		model.addAttribute("daftarUniv", daftarUniv);
    	return "prodi-form-insert";
    }
	
	 @RequestMapping(value = "/prodi/insert/submit", method = RequestMethod.POST)
	 public String insertSubmit(Model model, @ModelAttribute ProdiModel prodi, BindingResult result){
		 validator.validate(prodi, result);
		 
		 if(result.hasErrors()){
			throw new RuntimeException("error validate");
		 } else{
			 prodiService.insertProdi(prodi);
			 model.addAttribute("message", "Prodi Berhasil ditambahkan.");
			 return "success-notification";			 
		 }

    }
}
