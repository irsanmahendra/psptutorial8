package org.fasilkom.psp.tugas1.controller;

import org.fasilkom.psp.tugas1.model.PesertaModel;
import org.fasilkom.psp.tugas1.service.PesertaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestControllerPeserta {

	@Autowired
	private PesertaService pesertaService;

	@RequestMapping(value = "/rest/peserta", method = RequestMethod.GET)
	public PesertaModel peserta(Model model, @RequestParam(value = "nomor",  required = false) String nomor) {
		PesertaModel peserta = pesertaService.inquiryPeserta(nomor);
		return peserta;
	}
}
