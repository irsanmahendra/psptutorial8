package org.fasilkom.psp.tugas1.controller;

import org.fasilkom.psp.tugas1.model.PesertaModel;
import org.fasilkom.psp.tugas1.service.PesertaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class PengumumanController {

	@Autowired
	private PesertaService pesertaService;
	
    @RequestMapping(value = "/pengumuman/submit", 
    		method = RequestMethod.POST)
    public String pengumumanSubmit(Model model, 
    		@RequestParam(value = "nomor", required = true) String nomor)
    {
    	PesertaModel peserta = pesertaService.inquiryPeserta(nomor);
    	
    	
    	if(peserta != null) {
    		Integer umur = pesertaService.hitungUmur(peserta.getTglLahir());
    		
    		model.addAttribute("peserta", peserta);
    		model.addAttribute("umur", umur);
    		return "hasil-pengumuman";
    	} else{
    		model.addAttribute("nomor", nomor);
    		return "peserta-not-found";
    	}
    }
	
}
