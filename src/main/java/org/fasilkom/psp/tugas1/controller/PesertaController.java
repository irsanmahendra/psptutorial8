package org.fasilkom.psp.tugas1.controller;

import java.util.List;

import org.fasilkom.psp.tugas1.model.PesertaModel;
import org.fasilkom.psp.tugas1.model.ProdiModel;
import org.fasilkom.psp.tugas1.service.PesertaService;
import org.fasilkom.psp.tugas1.service.ProdiService;
import org.fasilkom.psp.tugas1.util.GeneralHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class PesertaController {

	@Autowired
	private PesertaService pesertaService;
	
	@Autowired
	private ProdiService prodiService;
	
	@RequestMapping("/peserta")
    public String viewPesertai (Model model,
           @RequestParam("nomor") String nomor)
    {
    	PesertaModel peserta = pesertaService.inquiryPeserta(nomor);
    	
    	if(peserta != null) {
    		Integer umur = pesertaService.hitungUmur(peserta.getTglLahir());
    		
    		model.addAttribute("umur", umur);
    		model.addAttribute("peserta", peserta);
    		return "peserta-view";
    	} else{
    		model.addAttribute("nomor", nomor);
    		return "peserta-not-found";
    	}
    }
	
	@RequestMapping("/peserta/changeprodi/{nomor}")
    public String changeProdi (Model model, @PathVariable("nomor") String nomor)
    {
		PesertaModel peserta = pesertaService.inquiryPeserta(nomor);
    	
    	if(peserta != null) {
    		List<ProdiModel> daftarProdi = prodiService.selectAll();
    		model.addAttribute("peserta", peserta);
    		model.addAttribute("daftarProdi", daftarProdi);
    		return "peserta-form-change-prodi";
    	} else{
    		model.addAttribute("nomor", nomor);
    		return "peserta-not-found";
    	}
    }
	
	 @RequestMapping(value = "/peserta/changeprodi/submit", method = RequestMethod.POST)
	 public String changeProdiSubmit(Model model, 
			 @RequestParam("nomor") String nomor, @RequestParam("prodi.kodeProdi") String prodi){
		 PesertaModel peserta = new PesertaModel();
		 peserta.setNomor(nomor);
		 ProdiModel prod = new ProdiModel();
		 prod.setKodeProdi(prodi);
		 peserta.setProdi(prod);
		 
		 pesertaService.changeProdi(peserta);
		 model.addAttribute("message", GeneralHelper.CHANGE_PRODI_SUCCESS_NOTIF);
		 return "success-notification";
   }
	 
	@RequestMapping("/peserta/changestatus/{nomor}")
    public String changeStatus (Model model, @PathVariable("nomor") String nomor)
    {
		PesertaModel peserta = pesertaService.inquiryPeserta(nomor);
    	
    	if(peserta != null) {
    		List<ProdiModel> daftarProdi = prodiService.selectAll();
    		model.addAttribute("peserta", peserta);
    		model.addAttribute("daftarProdi", daftarProdi);
    		
    		Integer status = (peserta.getProdi() != null) ? 1 : 0;
    		model.addAttribute("status", status);
    		
    		return "peserta-form-change-status";
    	} else{
    		model.addAttribute("nomor", nomor);
    		return "peserta-not-found";
    	}
    }
	
	 @RequestMapping(value = "/peserta/changestatus/submit", method = RequestMethod.POST)
	 public String changeStatusSubmit(Model model, 
			 @RequestParam("nomor") String nomor, @RequestParam("status") String status, @RequestParam("prodi.kodeProdi") String prodi){
		 PesertaModel peserta = new PesertaModel();
		 ProdiModel prod = new ProdiModel();
		 
		 peserta.setNomor(nomor);
		 
		 if(status.equals("0")){
			 peserta.setProdi(null);
		 } else{
			 prod.setKodeProdi(prodi);
			 peserta.setProdi(prod);
		 }

		 pesertaService.changeProdi(peserta);
		 model.addAttribute("message", GeneralHelper.CHANGE_STATUS_SUCCESS_NOTIF);
		 return "success-notification";
   }
}
