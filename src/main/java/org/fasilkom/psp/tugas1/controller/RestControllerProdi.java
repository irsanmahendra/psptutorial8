package org.fasilkom.psp.tugas1.controller;

import org.fasilkom.psp.tugas1.model.ProdiModel;
import org.fasilkom.psp.tugas1.service.ProdiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RestControllerProdi {

	@Autowired
	private ProdiService prodiService;
	
	@RequestMapping(value = "/rest/prodi", method = RequestMethod.GET)
    public ProdiModel prodi (Model model,
           @RequestParam(value = "kode", required = false) String kode)
    {
    	ProdiModel prodi = prodiService.selectProdi(kode);
    	return prodi;
    	
    }
}
