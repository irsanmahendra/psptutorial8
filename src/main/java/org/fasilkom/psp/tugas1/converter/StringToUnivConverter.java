package org.fasilkom.psp.tugas1.converter;

import org.fasilkom.psp.tugas1.model.UnivModel;
import org.fasilkom.psp.tugas1.service.UnivService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class StringToUnivConverter implements Converter<String, UnivModel>{

	@Autowired
	private UnivService univService;
	
	@Override
	public UnivModel convert(String arg0) {
		UnivModel univ = univService.selectUniv(arg0);
		return univ;
	}

}
